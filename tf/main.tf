resource "aws_instance" "web-server" {

  // AMI = Amazon Machine Image = .iso
  ami = "ami-0ff338189efb7ed37" // Ubuntu 20.04 LTS

  // Instance type defines resources allocated to the instance
  instance_type = "t3.micro" // 2 vCPUs, 1GB RAM

  // Instance access SSH key
  key_name = "demo-key"

  // Allocate public IP 
  associate_public_ip_address = true

  // Map security group
  depends_on      = [aws_security_group.web-server-sg]
  security_groups = [aws_security_group.web-server-sg.name]

  // It's considered a best practice to tag your stuff!
  tags = {
    Name        = "web-server-1"
    Terraform   = "true"
    Environment = "demo"
  }
}

resource "aws_security_group" "web-server-sg" {
  name = "web_server_sg"

  // Allow inbound port 80 
  ingress {
    from_port   = 0
    to_port     = 80
    protocol    = "tcp"
    cidr_blocks = ["0.0.0.0/0"]

  }

  // Allow inbound SSH
  ingress {
    from_port   = 0
    to_port     = 22
    protocol    = "tcp"
    cidr_blocks = ["0.0.0.0/0"] // Don't do this IRL, only allow what's needed! :)
  }

  // Allow outbound internet access.
  egress {
    from_port   = 0
    to_port     = 0
    protocol    = "-1"
    cidr_blocks = ["0.0.0.0/0"]
  }

}

// Print EC2 instance public IP after creation
output "ec2_public_ip" {
  value = ["${aws_instance.web-server.*.public_ip}"]
}

