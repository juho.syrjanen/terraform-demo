# NGINX installation & config



This simple Ansible playbook will target the virtual machines in the `inventory.yml` -file and:

- Install and start NGINX web server

- Copy demo website to `/etc/nginx/sites-available` and create a link to `/etc/nginx/sitest-enabled`

- Reload NGINX configuration using Ansible handler


Run playbook directly from the command line:

```shell
ansible-playbook -i inventory.yml --private-key=~/.ssh/demo-key -u ubuntu nginx.yml
```
