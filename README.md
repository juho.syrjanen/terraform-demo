# Infra As Code demo

Hello! This is a demo repo for a simple infra as code (IAC) setup. 
The goal is to automate deployment and configuration of web server in Amazon Web Services.

## Requirements

- awscli

- Terraform >v1.0.9

- Ansible

## Config

- Create AWS IAM user with programmatic access, retrieve access- & secret key.

- Run `aws configure` and pass access- & secret key 

- Run `terraform init` in `tf/` folder

## Deploy

```shell
cd tf/
terraform plan
terraform apply
```

## Cleanup

```shell
terraform destroy
```

## Webserver installation

See [Ansible](https://gitlab.com/juho.syrjanen/terraform-demo/-/tree/main/ansible) -folder's `README.md` .
